// Package v1alpha1 contains API Schema definitions for the tomergroup v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=tomergroup.okto.io
package v1alpha1
