package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TomerOpSpec defines the desired state of TomerOp
// +k8s:openapi-gen=true
type TomerOpSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
	 Firstname string `json:"firstname"`
	 Lastname string `json:"lastname"`
}

// TomerOpStatus defines the observed state of TomerOp
// +k8s:openapi-gen=true
type TomerOpStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
	Message string `json:"message"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TomerOp is the Schema for the tomerops API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type TomerOp struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TomerOpSpec   `json:"spec,omitempty"`
	Status TomerOpStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TomerOpList contains a list of TomerOp
type TomerOpList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TomerOp `json:"items"`
}

func init() {
	SchemeBuilder.Register(&TomerOp{}, &TomerOpList{})
}
