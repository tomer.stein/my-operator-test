package controller

import (
	"github.com/tomerop/pkg/controller/tomerop"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, tomerop.Add)
}
